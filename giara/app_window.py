from giara.constants import APP_ID, APP_NAME, PROFILE
from giara.confManager import ConfManager
from giara.main_ui import MainUI
from giara.base_app import AppShortcut, BaseWindow


class AppWindow(BaseWindow):
    def __init__(self):
        super().__init__(
            app_name=APP_NAME,
            icon_name=APP_ID + ('.dev' if PROFILE == 'development' else ''),
            shortcuts=[
                AppShortcut('F10', self.open_popover)
            ]
        )
        self.confman = ConfManager()
        self.reddit = self.confman.reddit

        self.main_ui = MainUI()
        self.append(self.main_ui)

        self.confman.connect(
            'dark_mode_changed',
            lambda *args: self.set_dark_mode(self.confman.conf['dark_mode'])
        )
        self.set_dark_mode(self.confman.conf['dark_mode'])

    def present(self):
        super().present()
        self.set_default_size(
            self.confman.conf['windowsize']['width'],
            self.confman.conf['windowsize']['height']
        )

    def open_popover(self, *args):
        self.main_ui.deck.left_stack.get_headerbar().menu_btn.popup()

    def emit_destroy(self, *args):
        self.emit('destroy')

    def on_destroy(self, *args):
        self.confman.conf['windowsize'] = {
            'width': self.get_width(),
            'height': self.get_height()
        }
        self.hide()
        self.confman.save_conf()

    def do_startup(self):
        pass
